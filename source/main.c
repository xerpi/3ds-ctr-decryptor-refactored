#include "3dstypes.h"
#include "3ds.h"
#include "draw.h"
#include "libc.h"
#include "crypto.h"
#include "FS.h"

/*
4 bytes number of entries
entry:
    16 bytes CTR
    16 bytes keyY
    4 bytes length in megabytes(round up)
    64 bytes output filename in utf16(could use something like "/ncchcode1.romfs.xorpad")

#define entry_ctr 0+4 //+4 because first int in the array is not an entry
#define entry_keyY 16+4
#define entry_megabytes 32+4
#define entry_filename 36+4
*/

struct ncch_info_entry {
    uint8_t  CTR[16];
    uint8_t  keyY[16];
    uint32_t size_mb;
    wchar_t  filename[32]; 
} __attribute__((packed));

struct ncch_info {
    uint32_t n_entries;
    struct ncch_info_entry entries[20];
} __attribute__((packed, aligned(16)));

static const uint8_t zero_buf[16] __attribute__((aligned(16))) = {0};

void ncch_info_createpad(struct ncch_info_entry *info)
{
    #define BUFFER_ADDR ((volatile uint8_t*)0x21000000)
    #define BLOCK_SIZE  (4*1024*1024)
    
    uint32_t handle = FileOpen(info->filename);
    setup_aeskey(0x2C, AES_BIG_INPUT|AES_NORMAL_INPUT, info->keyY);
    use_aeskey(0x2C);
    
    uint8_t ctr[16] __attribute__((aligned(32)));
    memcpy(ctr, info->CTR, 16);
    
    uint32_t size_bytes = info->size_mb*1024*1024;
    uint32_t size_100 = size_bytes/100;
    uint32_t i, j;
    for (i = 0; i < size_bytes; i += BLOCK_SIZE) {
        for (j = 0; (j < BLOCK_SIZE) && (i+j < size_bytes); j+= 16) {
            set_ctr(AES_BIG_INPUT|AES_NORMAL_INPUT, ctr);
            aes_decrypt((void*)zero_buf, (void*)BUFFER_ADDR+j, ctr, 1);
            add_ctr(ctr, 1);
        }
        draw_fillrect(SCREEN_TOP_W-33, 1, 32, 8, BLACK);
        font_draw_stringf(SCREEN_TOP_W-33, 1, CYAN, "%i%%", (i+j)/size_100);
        
        //FlushProcessDataCache(0xFFFF8001, (void*)BUFFER_ADDR, j);
        FileWriteOffset(handle, (void*)BUFFER_ADDR, j, i);
    }
}

int main()
{
    ClearScreen();
    DEBUG("3DS CTR DECRYPTOR by VOiD, refactored by xerpi"); 
    
    struct ncch_info info;
    
    DEBUG("Opening sd:/ncchinfo.bin ...");
    uint32_t handle = FileOpen(L"/ncchinfo.bin");
    DEBUG("Opened! Reading info...");
    FileRead(handle, &info, 16); //Read number of entries
    DEBUG("Number of entries: %i", info.n_entries);
    
    if (!info.n_entries || info.n_entries > 20) {
        ERROR("Nothing to do.:/ (%i)", info.n_entries);
        return 0;
    }
    
    FileRead(handle, &info, info.n_entries*sizeof(struct ncch_info_entry)+4);
    
    char str[32];
    int i;
    for(i = 0; i < info.n_entries; i++) {
        wstr_to_str(info.entries[i].filename, str);
        DEBUG("Creating pad number: %i  size (MB): %i", i+1, info.entries[i].size_mb);
        DEBUG("\tFilename: %s", str);
        ncch_info_createpad(&info.entries[i]);
        DEBUG("\tDone!");
    }

    DEBUG("Finished! You can turn off your 3DS now :P");
    return 0;
}

